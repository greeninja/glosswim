---
title: Policies and Documents
---
As a club, we are affiliated to Swim England and as such, we are expected to conduct our activities within their rules. These rules, including the Code of Conduct, Code of Ethics, an Equality Policy. Safeguarding and Child Welfare policy can be found here:

http://www.swimming.org/swimengland/wavepower-child-safeguarding-for-clubs/

Gloucester City Swimming Club is committed to regularly review our policies and align them with the latest standards The current policies are listed below:

{{< docs "https://drive.google.com/embeddedfolderview?id=1ts3kn1-FUxkO-Lk4MNqotOQLQkIh25UP" >}}