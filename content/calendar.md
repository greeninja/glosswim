---
title: Calendar
---
## Session Changes

Additional dates for training, meets and galas can be found below.

{{< docs "https://drive.google.com/embeddedfolderview?id=1yRsigIFGckwnSl8MMCA3g1cb0EZq_Aj1" >}}

## Club Calendar

For more information about the events displayed below, please visit our contact page.

{{< docs "https://calendar.google.com/calendar/b/5/embed?showTitle=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;src=h6o7k0e57jetadiruu1g64hdu4%40group.calendar.google.com&amp;color=%238C500B&amp;ctz=Europe%2FLondon" >}}
