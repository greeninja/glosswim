---
title: GCSC Open Meets
---
## Upcoming Meets

Upcoming Open meets can be found on the [Club Calendar](/calendar) or [Home Page](/)

{{< upcoming >}}

## Location

GCSC’s main base is at the GL1 Leisure Centre in Gloucester, and this is where the Gloucester Meets and galas take place. The address and directions can be found via google maps below.

{{< docs "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9855.655371188337!2d-2.2385908!3d51.8625295!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6c843f818eb9cf1d!2sGL1+Leisure+Centre!5e0!3m2!1sen!2suk!4v1550198803479" >}}

## Contact

If you'd like to contact us about our meets please use our contact form or the address below.

Email: swimgloscity@gmail.com

## Sponsorship

GCSC invites sponsorship of its open meets. If you represent a company that is interested in sponsoring one of our meets or you would like to pass on the contact details of a potential sponsor, please contact us at                       and we will contact you to discuss sponsorship options.

## Meet Results

Please view the results from our meets below.

{{< docs "https://drive.google.com/embeddedfolderview?id=1Esdv41ar4q56tuBqSj5jRjgxirAm12Ov" >}}