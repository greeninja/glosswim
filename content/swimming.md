---
title: Competitive Swimming
---
The competitive swimming pathway through the club consists of eight squads progressing from 4 hours training per week up to 17 hours. These eight squads are split into two distinct sections:  Swim Skills (SSK) and Train to Compete (TTC). This pathway aims to introduce key training concepts and strategies at the optimal time within a child’s maturation and development to achieve their full competitive potential. The pathway focuses on long term success and is based on the Athlete Development Support Pathway and Youth Development model as endorsed by Swim England and British Swimming. Movements between each squad are at the coach’s discretion.

## SSK4

In this introductory squad, swimmers are introduced to some basic coaching drills aiming to develop an understanding of how they are able to move their bodies in different ways with control. Competitions to enter:

* Club Championships – 50m events butterfly, backstroke, breaststroke, freestyle. 200m freestyle
* Four Seasons – 25m and 50m events (coach will select races)

These events are chosen for a gentle and friendly introduction to competitive swimming. For the remaining SSK squads 200m events should be the priority as success in these events requires a high level of stroke skills whereas success in 50m events is often influenced more by physical size rather than skill.

## SSK6

Swimmers are now required to train on a Saturday morning.  This is their first taste of an aerobic training session and will start to use training skills such as using the pace clock for turnaround times. Swimmers in this squad are expected to have all four legal strokes so that they are able to begin entering open meets. Their focused event is 200IM. This squad will now begin land training as a supplement to the swim programme. Land training at this early stage aims to develop flexibility, stability, core strength and kinaesthetic awareness, thus improving movement patterns and stroke technique when they swim. All training sessions are conducted by professional, qualified and highly experienced strength and conditioning coaches. Competitions to enter:

* Club Championships - 50m events fly, bk, brs, free. 200m events IM, bk, brs, free
* Four Seasons – 25s, 50s (coach will select races)
* L3 open meets – Required to qualify for Counties.  Use Club Champs times as a starting point, qualifying for 200IM should be primary focus.
* Gloucestershire County Championships – Please enter all events that you qualify for.

## SSK7

Swimmers in this squad will be refining stroke drills and practices to improve the efficiency of each stroke. Training will become steadily more progressive, increasing the swimmer’s aerobic capacity. By working with this core component of fitness, the swimmer’s ability to maintain skill over distance will be improved. Land training will still be focused on fundamental movement patterns to assist in the development of stroke technique. Competitions to enter:

* Club Championships – 50m events fly, bk, brs, free. 200m events IM, bk, brs, free
* L3 and L2 open meets – Required to qualify for Counties. Use Club Champs times as a starting point, qualifying for 200m IM should be primary focus. Look to introduce 400m free.
* Gloucestershire County Championships – Please enter all events that you qualify for.
* Looking to drop Four Seasons gala (may still be selected in order to field a complete team).

## SSK9

Swimmers will begin to have an equal balance of stroke improvement and aerobic work by practising skills with more volume. The aim is for these swimmers to complete around 16km per week. Land training will still be focused on fundamental movement patterns to assist in the development of stroke technique. Competitions to enter:

* Club Championships - 50m events fly, bk, brs, free. 200m events IM, bk, brs, free.
* L3 and L2 open meets – Required to qualify for Counties. Use Club Champs times as a starting point, qualifying for 200IM should be primary focus. Look to introduce 400m IM and 800m/1500m free.
* Gloucestershire County Championships – Please enter all events that you qualify for.
* Looking to drop Four Seasons gala (may still be selected in order to field a complete team).

## SSK12

As the last squad in the SSK section, its primary focus is to prepare the swimmers for the TTC section of the club. Swimmers will begin some greater aerobic development and focus on developing 400IM as their main event. Competitions to enter:

* Club Championships - 50m events fly, bk, brs, free. 200m events IM, bk, brs, free
* L3 and L2 open meets – Required to qualify for Counties. Use Club Champs times as a starting point, qualifying for 200m and 400m IM should be primary focus. Look to maintain a range of different events.
* L1 open meets. These are very high standard meets, please speak to your coach before entering.
* Gloucestershire County Championships – Please enter all events that you qualify for.
* South West Regional Championships - Please enter all events that you qualify for.

## TTC14

The primary focus of this squad is to build the swimmers' aerobic capacity whilst maintaining the skills developed in the SSK section. Swimmers in this squad are on the performance pathway and there are significantly more demands placed upon them whilst they focus on achieving qualifying times for the next level of competition. Their focused event remains the 400IM. This squad is invited to additional long course training once per month at Hengrove in Bristol (dates will vary, please check monthly training schedule and session changes). Long course training is designed and programmed to supplement the training programme and does not replace any of the normal training sessions. Competitions to enter:

L3 and L2 open meets – Required to qualify for Counties. Use Club Champs times as a starting point, qualifying for 200m and 400m IM should be primary focus.
Look to maintain a range of different events.

* L1 open meets. These are very high standard meets, please speak to your coach before entering.
* Gloucestershire County Championships – Please enter all events that you qualify for.
* South West Regional Championships - Please enter all events that you qualify for.
* Home Nation and British National Summer Championships - Please enter all events that you qualify for.

## TTC16

As one of the senior squads in the club, TTC16 has an element of performance focus. However, this is not about winning medals but about achieving the right processes throughout their races. The Senior squads (TTC16 and TTC17) have greater differentiation in their programme as the individual needs of each swimmer are catered for. Throughout their training week, the squads will often be split by gender, stroke and event distance. The swimmers are split by the coach’s discretion on an individual basis. This squad is invited to additional long course training once per month at Hengrove in Bristol (dates will vary, please check monthly training schedule and session changes). Long course training is designed and programmed to supplement the training programme and does not replace any of the normal training sessions. Competitions to enter:

L3 and L2 open meets – Required to qualify for Counties.
Use Club Champs times as a starting point, qualifying for 200m and 400m IM should be primary focus.
Look to maintain a range of different events.
L1 open meets. These are very high standard meets, please speak to your coach before entering.

* Gloucestershire County Championships – Please enter all events that you qualify for.
* South West Regional Championships - Please enter all events that you qualify for.
* Home Nation and British National Summer Championships - Please enter all events that you qualify for.

## TTC17

As one of the senior squads in the club, TTC17 has a performance focus.  Swimmers are now looking to use the processes they have learnt in training in order to succeed in a performance setting. The senior squads (TTC16 and TTC17) have greater differentiation in their programme as the individual needs of each swimmer are catered for. Throughout their training week, the squads will often be split by gender, stroke and event distance. The swimmers are split by the coach’s discretion on an individual basis. This squad is invited to additional long course training once per month at Hengrove in Bristol (dates will vary, please check monthly training schedule and session changes). Long course training is designed and programmed to supplement the training programme and does not replace any of the normal training sessions. Competitions to enter:

L3 and L2 open meets – Required to qualify for Counties.
Use Club Champs times as a starting point, qualifying for 200m and 400m IM should be primary focus.
Look to maintain a range of different events.

* L1 open meets. These are very high standard meets, please speak to your coach before entering.
* Gloucestershire County Championships – Please enter all events that you qualify for.
* South West Regional Championships - Please enter all events that you qualify for.
* Home Nation and British National Summer Championships - Please enter all events that you qualify for.

## Non-Competitive Swimming – Train to Enjoy (TTE)

Not looking to swim competitively? No problem, check out our Training to Enjoy option below.

{{< swimmers >}}
