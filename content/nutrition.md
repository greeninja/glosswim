---
title: Nutrition
---
Nutrition is key to supporting the growth and development of swimmers, with the main focus ensuring that swimmers recover fully from one training session and will be ready for the next.

Below is the nutritional information provided by Swim England.

{{< docs "https://drive.google.com/embeddedfolderview?id=10YXgADrIas67COQKQKckn5DY6RBvPiQE" >}}