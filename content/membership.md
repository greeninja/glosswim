---
title: Membership
---
## New Members

The club has compiled this membership information pack to help swimmers and parents understand some of the basic requirements that are essential when joining a competitive swimming club. The information in this pack is only a brief guide and should you have any questions regarding membership please contact the Membership Secretary.

### New Membership Enquiries

Please complete the following form and email it through to: Membership@Gloucestercityswimmingclub.co.uk

{{< docs "https://drive.google.com/embeddedfolderview?id=1QjhQbzLRWoBQOu6VtclUCgc5Jm-HS-oq" >}}

Please note that whilst you may complete the form electronically (using the Word version), it will need to be printed off and signed – and either brought to the trial with your son/daughter – or scanned and emailed through to:

Membership@Gloucestercityswimmingclub.co.uk

### Training Fees

Training fees are payable monthly at the beginning of each calendar month, for that month. We require fees to be paid by monthly standing order, September to August inclusive. The monthly payment will vary depending on which squad your child trains in and the number of sessions per week available to him/her. The payment takes into account Bank Holidays, Seasonal breaks and the occasional scheduled and unscheduled cancellation due to Gala’s etc. A standing order form can be obtained from the Membership Secretary.

### Membership Fees

There are two types of membership which you must have in order to be a member of Gloucester City SC (GCSC).

These are as follows:

* Membership of Gloucester City SC (most of which goes towards insurance)
* Membership of the Swim England (SE) and affiliation to Region and County

All members including parents of swimmers under the age of eighteen must be a member of Swim England (SE).

## Initial Assessment

If you would like to arrange a trial to join GCSC please contact the Membership Secretary.

The initial assessment will be carried out by one of the club’s fully qualified coaches, often within a normal training session.

The assessment will consider the following:

* Age
* Confidence in the water
* Ability to perform all strokes

Following the assessment the coach will decide if the swimmer is ready to join GCSC and which squad/sessions would be suitable. The coach will then inform the Membership Secretary.