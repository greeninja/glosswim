---
title: About Us
---
Information about GCSC to go here.

## Meet the Coaching Team

### Head Coach

James Richards is the Head Coach of Gloucester City Swimming Club and is responsible for all programmes across the club, though his focus in on developing and supporting TTC16 and TTC17 swimmers through to National level. He is a Level 3 Senior Coach with a Master’s degree investigating swimming warm ups and a 1st Class Honours degree in Applied Sport and Exercise Studies. Over the past 9 years since joining the Club, James has worked with swimmers of all stages and focuses on a long-term approach to success. James uses a scientific approach to training and is keen to stay up to date with the latest academic research. James has developed swimmers to British Championships and National Competitions and has also coached a swimmer to achieve a British Disability Record for 1500m Freestyle (Classification S8). James is regularly involved with the England Talent Team, taking the Head Coach position for the Swim England County Development Camps, Skills Coach positions for Swim England National Phase 2 Camps as well as being involved with an overseas training camp and an international competition.

### Development Coach

Lily Lowe is the Development Coach at Gloucester City Swimming Club, focused on supporting SSK12 and TTC14 swimmers through to Regional level. She is a Swim England level 2 teacher with experience in top stage and competitive swimming. She was previously coaching age group and development swimmers privately and with Ledbury District Amateur Swim Club, where she also used to swim competitively. Lily is passionate about swimming and is always researching new developments in the sport, specifically stroke technique. In coaching she aims to look at the limitations and strengths of swimmers’ bodies to build the best stroke technique for each individual. Her approach to coaching is very goal-focused with the aim of guiding swimmers on how to make the most of their sets, understand their training and build deliberate practice into all training sessions. Lily is also a qualified yoga instructor and brings this experience into her coaching, looking to improve mobility and range of movement, breath-control and mental attitude.

### Swim Skills Lead Coach

Craig Skinner is the Swim Skills Lead Coach at Gloucester City Swimming Club and is focused on developing stroke technique and racing skills for SSK4, SSK6, SSK7 and SSK9 groups. He also leads the Open Water training programme. Craig has been involved in swimming since a young age and successfully competed for Tewkesbury Swimming Club at National and International competitions. When he became Head Coach at Tewkesbury SC, he developed young swimmers up to Regional level. With a passion for coaching and progressing the next generation of athletes, Craig has also advanced his understanding of athletic development through building his personal training company.

### Para Lead Coach



### Assistant Coach

### Strength and Conditioning Coaches

## Meet the Committee

Information about our committee and committee members