---
title: Galas & Open Meets
---

The Fixtures and Events for Gloucester City Swimming Club can be viewed from the calendar section of the club [here](/calendar)

This information is updated to reflect target meets for GCSC swimmers on a regular basis including Team competitions.

## Gala Guide

Please click on the link below to view our comprehensive guide to galas

{{< docs "https://drive.google.com/embeddedfolderview?id=1UW0Wd_G5X5_t58II-z4gGpN9M2mJ2EEu" >}}

## Qualifying Times

Please click on the link below to view the published qualifying times for the County Championships.

{{< docs "https://drive.google.com/embeddedfolderview?id=1GW8sV3pVOj6iC2P6sFsMHmrEg1c9CfDH" >}}

## Gala Results

Please view the results from Galas below.

{{< docs "https://drive.google.com/embeddedfolderview?id=1Esdv41ar4q56tuBqSj5jRjgxirAm12Ov" >}}

## Open Meet Packs

Gala Reports can be viewed below

{{< docs "https://drive.google.com/embeddedfolderview?id=1PgG1349aaOcxROpPZeBx5bh0mDD9EMXI" >}}


