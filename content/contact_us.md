---
title: Get in touch
---
Have any questions? Please get in touch with us using the form below.

{{< form >}}

## Club Committee Members Email Addresses

**Chairman**
Email: chair@gloucestercityswimmingclub.co.uk

**Treasurer**
Email: gcsctreasurer@gmail.com

**Secretary**
Email: gcscsecretary1@gmail.com

**Welfare Office**
Email: Welfare@Gloucestercityswimmingclub.co.uk

**Membership Secretary**
Email: Membership@Gloucestercityswimmingclub.co.uk

**Competitions Secretary**
Email: Fixtures@Gloucestercityswimmingclub.co.uk

**Workforce Co-ordinator**
Email: Workforce@Gloucestercityswimmingclub.co.uk

**Communication Officer**
Email: gcsccommunications@gmail.com