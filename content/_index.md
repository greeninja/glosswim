---
title: Why swim with us?
---
{{< card "Why swim with us?" "contact_us" "Get in Touch" >}}
Our primary objective is to create a fun, positive and happy environment for children and young adults to learn and enjoy competitive swimming. The club’s secondary objective is to provide swimmers with the right opportunities throughout their development in order to achieve success and fulfil their true potential in competitive swimming.

Gloucester City Swimmers will be:

* Encouraged to fulfil their potential as an athlete and to develop their personal and life skills.  
* Encouraged to see that hard work and honest endeavour achieve success.
* Taught to understand the value of supporting their team and the value of support from their team.
* Constantly challenged with small goals that are within the athlete’s control.
* Taught to win with humility, lose with resolve and to embrace the commitment to always do their personal best.

{{< /card >}}

{{< card "Upcoming Events" "/upcoming_events" "Find Out More" "shortcode" >}}
{{< upcoming >}}
{{< /card >}}

{{< rawhtml >}}

<div class="card-group mt-2 mb-2 ml-2 mr-2">
{{< /rawhtml >}}

{{< multi_card "Swimming" "swimming" "Find Out More" >}}
The competitive swimming pathway through the club consists of eight squads progressing from 4 hours training per week up to 17 hours. These eight squads are split into two distinct sections; Swim Skills (SSK) and Train to Compete (TTC).
{{< /multi_card >}}

{{< multi_card "Para-Swimming" "para_swimming" "Find Out More" >}}
The approach for this squad is to work with the individual needs and abilities of the swimmer, focusing on what they can do rather than what they can’t. Swimmers will focus on developing stroke technique and racing skills.
{{< /multi_card >}}

{{< multi_card "Open Water Swimming" "openwater" "Find Out More" >}}
Open water sessions are run once a month at Croft Farm in Tewkesbury 5.30-6.30pm between April and July (dates will vary, please check monthly training schedule and session changes).
{{< /multi_card >}}

{{< rawhtml >}}

</div>
{{< /rawhtml >}}