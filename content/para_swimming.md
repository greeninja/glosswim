---
title: Para Swimming
---

The approach for this squad is to work with the individual needs and abilities of the swimmer, focusing on what they can do rather than what they can’t. Swimmers will focus on developing stroke technique and racing skills. Once swimmers have reached a high training standard the coaching team will aim to integrate them into other squads.

Classification: For physical impairments, the process is as follows:

The swimmer ensures that they have medical documentation that has a diagnosis, so we can make sure that the impairment is eligible for Para-Swimming. The swimmer completes a classification application form once they are a CAT 2 member and training consistently and the coach has ensured that the swimmer is able to swim all strokes (impairment permitting), to a good standard.
A Swim England Para Talent Coach will then observe the swimmer to ensure that their swimming is at the required skill level to ensure classifiers can make a judgement based on the impairment rather than lack of training / skills.
Once signed off by a Swim England Para Talent Coach, the swimmer can send the form to British Para-Swimming classification and will then be asked for further medical documentation.
The swimmer will be placed on the waiting list for classification and they will then enter a competition where classification is taking place. If a slot is available, they will be allocated a slot and they will be seen in the morning or afternoon of the competition.
The competitions are generally the Regional Para-Swimming Championships.

For Intellectual Impairments, the specific criteria is:

* Requiring an IQ of 75 or below
* Requiring social adaptation
* Proof of onset before the age of 18.

The process is as follows:

Autism on its own isn’t eligible as there is often an IQ higher than 75. However, the only way to find out would be through a specific IQ test based on age, carried out by an educational psychologist.
There are two levels of classification, UKSA and INAS. INAS is the most recognised by British Swimming and needed to swim at County and Regional events.
(The Regional Para-Swimming and National Para-Swimming do allow the UKSA classification within those events).
INAS also requires the coach to complete an online form, (TSAL) saying how they adapt sessions for the swimmer.
The full procedures for both physical and intellectual classification process can be found on the British Swimming Website on the following link:

https://www.britishswimming.org/performance/para-swimming/classification/

Competitions to enter:

* Club Championships - 50m events fly, bk, brs, free. 200m events IM, bk, brs, free
* South West Regional Para-Swimming Championships
* Para Junior National Championships
* National Para-Swimming Championships
* International Para-Swimming Championships
* Gloucester County Championships
* South West Regional Championships
* Level 3 & Level 2 Open Meets that have a Multi-Classification element (all GCSC open meets do)
