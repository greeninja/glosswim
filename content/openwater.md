---
title: Open Water
---
Open water sessions are run once a month at Croft Farm in Tewkesbury 5.30-6.30pm between April and July (dates will vary, please check monthly training schedule and session changes). Any swimmers from SSK7 and above, Para and TTE are welcome to attend, subject to coach’s discretion. Open water sessions are in addition to the normal training programme and cost £5 per session. Renting a wetsuit will cost £3. Croft Farm will require you to sign a medical declaration before swimming as they run their centre without lifeguards.

Competitions to enter:

* Weston Super-Mare Open Water Development Series.
* Swim Serpentine
* Gloucester County/South West Regional Open Water Championships
* National Open Water Championships