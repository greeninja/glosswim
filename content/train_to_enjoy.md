---
title: Non-Competitive Swimming – Train to Enjoy (TTE)
---

This is a unique section within the club as the programme is driven exclusively by what the individual swimmer wants to achieve. The swimmers in this squad take a bigger ownership of their training and have many more options as to the direction they wish to take their own swimming. The primary focus is to enjoy swimming in a more relaxed environment without the pressure to perform in a competitive setting.

Swimmers in this section are more than welcome to enter any competitions or events that they qualify for and are encouraged to enter the Club Championships.
